#include "pch.h"

#include "CHostWPFWnd.h"

HWND CHostWPFWnd::GetWPFWindowHwnd()
{
	MainWindow^ wpfWindow = gcnew MainWindow();
	wpfWindow->Top = -100000;

	WindowInteropHelper^ wih = gcnew WindowInteropHelper(wpfWindow);

	wpfWindow->Show();

	HWND h = (HWND)(wih->Handle.ToPointer());
	return h;
}

void CHostWPFWnd::ShowWPFWindow(HWND hwnd)
{
	MainWindow^ wpfWindow = gcnew MainWindow();
	WindowInteropHelper^ wih = gcnew WindowInteropHelper(wpfWindow);
	wih->Owner = IntPtr(hwnd);
	wpfWindow->Show();
}
