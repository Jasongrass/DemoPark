#pragma once

using namespace System;
using namespace System::Windows;
using namespace System::Windows::Interop;
using namespace System::Runtime;
using namespace MyWpfApp;

public ref class CHostWPFWnd
{
public:
	static HWND GetWPFWindowHwnd();
	static void ShowWPFWindow(HWND hwnd);
};

