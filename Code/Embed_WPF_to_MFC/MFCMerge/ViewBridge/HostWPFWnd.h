#pragma once


using namespace System;
using namespace System::Windows;
using namespace System::Windows::Interop;
using namespace System::Runtime;
using namespace MyWPFApp;

public ref class HostWPFWnd
{
public:
    int GetHwnd(int parentHwnd)
    {
        // 在 C# 中获取到窗口句柄
        int ptr2 = ExportWindowHelper::GetMainWindow();
        // int ptr2 = ExportWindowHelper::GetWPFView(parentHwnd);
        return ptr2;

        // 在 C++/CLI 中获取窗口句柄
        //MainWindow^ wpfWindow = gcnew MainWindow();
        //WindowInteropHelper^ wih = gcnew WindowInteropHelper(wpfWindow);
        //wpfWindow->Top = -100000;

        //wpfWindow->Show();

        //int ptr = (int)(wih->Handle.ToPointer());

        //return ptr;
    }
};

