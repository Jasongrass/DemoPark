#pragma once

#ifdef VIEW_BRIDGE_EXPORTS
#define VIEW_BRIDGE_API __declspec(dllexport)
#else
#define VIEW_BRIDGE_API __declspec(dllimport)
#endif

#include <string>
#include <vector>

class VIEW_BRIDGE_API ViewBridgeWrapper
{
public:
    int GetHwnd(int parentHwnd);
};
