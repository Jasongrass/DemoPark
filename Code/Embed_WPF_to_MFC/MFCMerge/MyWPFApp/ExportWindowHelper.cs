﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Interop;

namespace MyWPFApp
{
    public static class ExportWindowHelper
    {
        private static MainWindow? _mainWindow;
        private static int _mainWindowPtr;

        public static int GetMainWindow()
        {
            if (_mainWindow == null)
            {
                _mainWindow = new MainWindow { Top = -100000 };
                _mainWindow.Show();

                var interopHelper = new WindowInteropHelper(_mainWindow);

                _mainWindowPtr = (int)(interopHelper.Handle);
            }

            return _mainWindowPtr;
        }

        public static int GetWPFView(int parentHwnd)
        {
            try
            {
                var param = new HwndSourceParameters();
                param.PositionX = 50;
                param.PositionY = 50;
                param.Width = 1000;
                param.Height = 600;
                param.ParentWindow = new IntPtr(parentHwnd);
                param.WindowStyle = WS_VISIBLE | WS_CHILD;

                var panel = new StackPanel();
                var txt = new TextBox() { Width = 100, Height = 30 };
                panel.Children.Add(txt);

                var hs = new HwndSource(param);
                hs.RootVisual = panel;

                return hs.Handle.ToInt32();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Debug.WriteLine(ex);
                return 0;
            }
        }

        const int WS_VISIBLE = 0x10000000;
        const int WS_CHILD = 0x40000000;
    }
}
