﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GrassDemoPark.WPF.UIAwaiter.Demo2Awaiter.Business;
using GrassDemoPark.WPF.UIAwaiter.Demo2Awaiter.UI;

namespace GrassDemoPark.WPF.UIAwaiter.Demo2Awaiter
{
    /// <summary>
    /// Window2Awaiter.xaml 的交互逻辑
    /// </summary>
    public partial class Window2Awaiter : Window
    {
        private FlowController _flowController;

        public Window2Awaiter()
        {
            InitializeComponent();
            Loaded += OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            // 初始化 UI Awaiter 测试代码
            var userInputViewHandler = new UserInputViewHandler(this.UserInputView);
            _flowController = new FlowController(userInputViewHandler);
        }

        private async void StartFlowButton_OnClick(object sender, RoutedEventArgs e)
        {
            UserInputView.Visibility = Visibility.Visible;

            MessageTextBlock.Text = "等待用户输入";
            var processResult = await _flowController.DoFlowAsync();
            MessageTextBlock.Text = "处理完成，结果是：" + processResult;

            UserInputView.Visibility = Visibility.Collapsed;
        }
    }
}
