﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GrassDemoPark.WPF.UIAwaiter.Demo2Awaiter.Business
{
    class FlowController
    {

        private IUserInputViewHandler UserInputViewHandler { get; }

        public FlowController(IUserInputViewHandler userInputViewHandler)
        {
            this.UserInputViewHandler = userInputViewHandler;
        }

        public async Task<string> DoFlowAsync()
        {
            Step1();
            var inputContent = await UserInputViewHandler.GetUserInputAsync();
            var precessResult = Step2(inputContent);
            StepEnd();
            return precessResult;
        }


        private void Step1()
        {

        }

        private string Step2(string userInputContent)
        {
            return $"###{userInputContent}###";
        }

        private void StepEnd()
        {
        }
    }
}
