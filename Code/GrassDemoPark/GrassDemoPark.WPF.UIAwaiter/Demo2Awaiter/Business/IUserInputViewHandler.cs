﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GrassDemoPark.WPF.UIAwaiter.AwaiterApi;

namespace GrassDemoPark.WPF.UIAwaiter.Demo2Awaiter.Business
{
    interface IUserInputViewHandler
    {
        DispatcherAsyncOperation<string> GetUserInputAsync();
    }
}
