﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GrassDemoPark.WPF.UIAwaiter.AwaiterApi;

namespace GrassDemoPark.WPF.UIAwaiter.Demo2Awaiter.UI
{
    /// <summary>
    /// UserInputView.xaml 的交互逻辑
    /// </summary>
    public partial class UserInputView : UserControl
    {

        // 方案2 使用 awaiter
        private Action<string, Exception> _reportResult; // 这里的 string，是需要返回的数据的类型

        public UserInputView()
        {
            InitializeComponent();
        }

        private void OkButton_OnClick(object sender, RoutedEventArgs e)
        {
            // 基本的输入校验
            if (string.IsNullOrWhiteSpace(InputTextBox.Text))
            {
                MessageBox.Show("输入不能为空");
                return;
            }

            // 方案2 上报用户输入，调用这里之后，对 GetUserInputAsync 的调用才会返回结果。
            _reportResult?.Invoke(InputTextBox.Text, null);
        }

        // 方案2 使用方法对外提供用户输入的值
        public DispatcherAsyncOperation<string> GetUserInputAsync()
        {
            return DispatcherAsyncOperation<string>.Create(out _reportResult);
        }

    }
}
