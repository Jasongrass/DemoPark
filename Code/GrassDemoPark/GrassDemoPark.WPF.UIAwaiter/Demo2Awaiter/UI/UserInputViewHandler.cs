﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GrassDemoPark.WPF.UIAwaiter.AwaiterApi;
using GrassDemoPark.WPF.UIAwaiter.Demo2Awaiter.Business;

namespace GrassDemoPark.WPF.UIAwaiter.Demo2Awaiter.UI
{
    class UserInputViewHandler : IUserInputViewHandler
    {
        private UserInputView UserInputView { get; }

        public UserInputViewHandler(UserInputView userInputView)
        {
            UserInputView = userInputView;
        }

        // 方案2 使用方法对外提供用户输入的值
        public DispatcherAsyncOperation<string> GetUserInputAsync()
        {
            return UserInputView.GetUserInputAsync();
        }

    }
}
