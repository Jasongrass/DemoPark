﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GrassDemoPark.WPF.UIAwaiter.Demo1Event.Business
{
    class FlowController
    {
        private bool _isUnderFlowing = false;

        private IUserInputViewHandler UserInputViewHandler { get; }

        public FlowController(IUserInputViewHandler userInputViewHandler)
        {
            this.UserInputViewHandler = userInputViewHandler;
            UserInputViewHandler.UserInputFinished += OnUserInputFinished;
        }

        public void DoFlow()
        {
            _isUnderFlowing = true;
            Step1();
        }

        private void OnUserInputFinished(object sender, string inputContent)
        {
            // 状态判断，如有没有执行 Step1 等。如果状态判断OK，则执行 Step2。
            if (!_isUnderFlowing)
            {
                return;
            }
            var precessResult = Step2(inputContent);
            StepEnd();

            // 外部如何拿到 precessResult？
        }

        private void Step1()
        {

        }

        private string Step2(string userInputContent)
        {
            MessageBox.Show($"User Input:{userInputContent}");
            return $"###{userInputContent}###";
        }

        private void StepEnd()
        {
            _isUnderFlowing = false;
        }
    }
}
