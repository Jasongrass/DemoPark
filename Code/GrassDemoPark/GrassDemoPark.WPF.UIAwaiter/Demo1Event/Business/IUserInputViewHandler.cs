﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GrassDemoPark.WPF.UIAwaiter.AwaiterApi;

namespace GrassDemoPark.WPF.UIAwaiter.Demo1Event.Business
{
    interface IUserInputViewHandler
    {
        event EventHandler<string> UserInputFinished;

    }
}
