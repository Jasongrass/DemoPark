﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GrassDemoPark.WPF.UIAwaiter.Demo1Event.Business;
using GrassDemoPark.WPF.UIAwaiter.Demo1Event.UI;

namespace GrassDemoPark.WPF.UIAwaiter.Demo1Event
{
    /// <summary>
    /// Window1Event.xaml 的交互逻辑
    /// </summary>
    public partial class Window1Event : Window
    {
        private FlowController _flowController;

        public Window1Event()
        {
            InitializeComponent();
            Loaded += OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            // 初始化 UI Event 测试代码
            var userInputViewHandler = new UserInputViewHandler(this.UserInputView);
            _flowController = new FlowController(userInputViewHandler);
        }

        private void StartFlowButton_OnClick(object sender, RoutedEventArgs e)
        {
            UserInputView.Visibility = Visibility.Visible;

            MessageTextBlock.Text = "等待用户输入；现在的代码，不知道何时处理结束，还要加代码。";
            _flowController.DoFlow();
            // 想要在这里拿到 Flow 处理的最终结果，发现有点麻烦。
        }

    }
}
