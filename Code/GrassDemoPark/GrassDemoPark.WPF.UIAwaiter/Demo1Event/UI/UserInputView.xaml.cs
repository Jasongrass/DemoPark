﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GrassDemoPark.WPF.UIAwaiter.AwaiterApi;

namespace GrassDemoPark.WPF.UIAwaiter.Demo1Event.UI
{
    /// <summary>
    /// UserInputView.xaml 的交互逻辑
    /// </summary>
    public partial class UserInputView : UserControl
    {
        // 方案1 使用事件（对外公开事件，以通知用户的输入） 
        public event EventHandler<string> UserInputFinished;

        public UserInputView()
        {
            InitializeComponent();
        }

        private void OkButton_OnClick(object sender, RoutedEventArgs e)
        {
            // 基本的输入校验
            if (string.IsNullOrWhiteSpace(InputTextBox.Text))
            {
                MessageBox.Show("输入不能为空");
                return;
            }

            // 方案1 使用事件（对外公开事件，以通知用户的输入） 
            UserInputFinished?.Invoke(this, InputTextBox.Text);
        }

    }
}
