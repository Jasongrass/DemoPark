﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GrassDemoPark.WPF.UIAwaiter.Demo1Event.Business;

namespace GrassDemoPark.WPF.UIAwaiter.Demo1Event.UI
{
    class UserInputViewHandler : IUserInputViewHandler
    {
        // 方案1 使用事件（对外公开事件，以通知用户的输入） 
        public event EventHandler<string> UserInputFinished;

        private UserInputView UserInputView { get; }

        public UserInputViewHandler(UserInputView userInputView)
        {
            UserInputView = userInputView;
            UserInputView.UserInputFinished += (sender, inputContent) =>
            {
                UserInputFinished?.Invoke(this, inputContent);
            };
        }

    }
}
