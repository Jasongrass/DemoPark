﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GrassDemoPark.WPF.UIAwaiter.Demo1Event;
using GrassDemoPark.WPF.UIAwaiter.Demo2Awaiter;

namespace GrassDemoPark.WPF.UIAwaiter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void TestDemo1EventButton_OnClick(object sender, RoutedEventArgs e)
        {
            new Window1Event()
            {
                Owner = this
            }.ShowDialog();
        }

        private void TestDemo2AwaiterButton_OnClick(object sender, RoutedEventArgs e)
        {
            new Window2Awaiter()
            {
                Owner = this
            }.ShowDialog();
        }
    }
}
